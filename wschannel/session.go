package zellochannel

import (
	"github.com/gorilla/websocket"
	//	"github.com/hraban/opus"
	log "github.com/sirupsen/logrus"
	"time"
)

// Start a new connection to the server
func Connect(url string) (s *Session, err error) {
	log.Printf("connecting to %v", url)
	s = &Session{
		Sequence: 1,
	}
	s.WS, _, err = websocket.DefaultDialer.Dial(url, nil)
	if err != nil {
		log.Errorf("dial error: %v", err)
		return
	}
	s.WS.SetPingHandler(func(message string) error {
		log.Debugf("ping message %v", message)
		s.WS.WriteControl(websocket.PongMessage, []byte(message), time.Time{})
		return nil
	})

	s.Streams = make(map[uint32]*Stream, 16)
	s.RequestQueue = make(map[int]chan Response, 3)
	s.Channels = make(map[string]ChannelStatus)
	go s.Listen()
	return
}

// Close the session
func (s *Session) Disconnect() {
	// Cleanly close the connection by sending a close message and then
	// waiting (with timeout) for the server to close the connection.
	//			portaudio.Terminate()
	err := s.WS.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		log.Infof("write close: %v", err)
		return
	}

	select {
	//	case <-done:
	case <-time.After(time.Second):
		return
	}

	s.WS.Close()
	return
}

// Handle incoming messages in a go routine
func (s *Session) Listen() {
	//	defer close(done)
	log.Debug("Listening for messages")
	for {
		messagetype, message, err := s.WS.ReadMessage()
		if err != nil {
			log.Errorf("WS Read error: %v", err)
			return
		}
		if messagetype == 2 {
			HandleStream(s, message)
		} else {
			log.Debugf("recv: type %v %s", messagetype, message)
			HandleMessage(s, message)
		}
	}
}

// Perform server authentication and new session
func (s *Session) Logon(username, password, token string, channels []string) error {
	message := &Logon{
		Command:    "logon",
		AuthToken:  token,
		Username:   username,
		Password:   password,
		Channels:   channels,
		ListenOnly: false,
	}
	response, err := s.SendMessage(message)
	if err != nil {
		log.Fatalf("Could not log on to server %v", err)
	} else if !response.Success {
		log.Fatalf("Server login response is %v", response)
	}
	return err
}
