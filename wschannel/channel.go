package zellochannel

import (
	log "github.com/sirupsen/logrus"
	//	b64 "encoding/base64"
	//	"encoding/binary"
	"encoding/json"
)

func HandleMessage(s *Session, messagebyte []byte) {
	var messagemap map[string]interface{}
	err := json.Unmarshal(messagebyte, &messagemap)
	if err != nil {
		log.Errorf("Problem testing message %v", err)
	}
	seq, isreply := messagemap["seq"]
	if isreply {
		seqInt := int(seq.(float64))
		log.Infof("Message is a reply to seq %v", seqInt)
		var response Response
		err = json.Unmarshal(messagebyte, &response)
		if err != nil {
			log.Errorf("Problem unmarshalling reply %v", err)
		}
		s.RequestQueue[seqInt] <- response

	} else {
		switch messagemap["command"] {
		case "on_channel_status":
			log.Info("Channel status message")
			HandleStatusMsg(s, messagebyte)
		case "on_stream_start", "on_stream_stop":
			HandleStreamCommand(s, messagebyte)
		}

	}
}

func HandleStatusMsg(s *Session, message []byte) {
	var status ChannelStatus
	err := json.Unmarshal(message, &status)
	if err != nil {
		log.Errorf("Problem unmarshalling status %v", err)
		return
	}
	log.Warnf("Channel %v is %v with %v users", status.Channel, status.Status, status.UsersOnline)
	s.Channels[status.Channel] = status
	if s.StatusHandler != nil {
		s.StatusHandler(status)
	}
}

func HandleStreamCommand(s *Session, message []byte) {
	var command StreamCommand
	err := json.Unmarshal(message, &command)
	if err != nil {
		log.Errorf("Problem unmarshalling stream command %v", err)
		return
	}
	log.Infof("Channel %v command %v from %v ", command.Channel, command.Command, command.From)
	switch command.Command {
	case "on_stream_start":
		StreamStart(s, command)
	case "on_stream_stop":
		//		log.Info("Stream stopped")
		StreamStop(s, command)
	}
}
