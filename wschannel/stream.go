package zellochannel

import (
	//	b64 "encoding/base64"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
	"gopkg.in/hraban/opus.v2"
)

var (
//	Streams = make(map[uint32]*Stream, 16)
)

func StreamStart(s *Session, command StreamCommand) {
	var err error
	id := command.StreamID
	format := DecodeHeader(command.CodecHeader)
	format.Codec = command.Codec
	log.Infof("Header is %v", format)
	log.Infof("Creating stream object for ID %v", id)
	s.Streams[id] = &Stream{
		ID:     command.StreamID,
		Format: format,
	}

	s.Streams[id].Decoder, err = opus.NewDecoder(int(format.Rate), 1)
	if err != nil {
		log.Errorf("Problem opening Opus decoder %v", err)
		return
	}
	samples := int(format.Rate) * int(format.Interval) * int(format.Frames) / 1000
	log.Debugf("Recommended buffer size is %v based on format %s", samples, format)
	s.Streams[id].Buffer = make([]int16, samples)

	if s.StreamStartHandler != nil {
		s.StreamStartHandler(command, s.Streams[id])
	}
	//	log.Infof("Memory object for stream object %v is %v", id, Streams[id])

}

func StreamStop(s *Session, command StreamCommand) {
	log.Infof("Stopping stream %v", command.StreamID)
	if s.StreamStopHandler != nil {
		s.StreamStopHandler(command, s.Streams[command.StreamID])
	}
	delete(s.Streams, command.StreamID)
}

func HandleStream(s *Session, messagebyte []byte) {
	//	log.Infof("received stream frame of length %v", len(messagebyte))
	var messagetype uint8
	var stream_id uint32
	var packet_id uint32
	var data []byte
	//	time.Sleep(time.Second / 2)
	messagetype = messagebyte[0]
	if messagetype == 0x01 && len(messagebyte) > 9 {
		stream_id = binary.BigEndian.Uint32(messagebyte[1:5])
		packet_id = binary.BigEndian.Uint32(messagebyte[5:9])
		data = messagebyte[9:]
		log.Debugf("Got stream id %v packet %v of length %v ", stream_id, packet_id, len(data))

		//		s := Streams[stream_id]
		log.Debugf("Decoding to stream handle %v", s.Streams[stream_id].ID)
		//samples := s.Format.Rate * 1000 * uint16(s.Format.Interval) * uint16(s.Format.Frames) / 1000
		samplesdecoded, err := s.Streams[stream_id].Decoder.Decode(data, s.Streams[stream_id].Buffer)
		if err != nil {
			log.Errorf("Problem decoding frame from stream %v %v", stream_id, err)
		} else {
			s.Streams[stream_id].Audio = append(s.Streams[stream_id].Audio, s.Streams[stream_id].Buffer...)
			if s.PacketHandler != nil {
				s.PacketHandler(s.Streams[stream_id])
			}
			s.Streams[stream_id].Index = s.Streams[stream_id].Index + uint32(samplesdecoded)
			s.Streams[stream_id].Length += uint32(samplesdecoded)
			log.Debugf("Samples in this packet is %v", samplesdecoded)
		}
	} else {
		log.Errorf("Problem with stream data - type is %v and length is %v", messagetype, len(messagebyte))
	}

}

func (s *Session) OpenChannel(channel string, format StreamType) (id uint32, err error) {
	command := &StreamCommand{
		Command:        "start_stream",
		Channel:        channel,
		StreamType:     "audio",
		Codec:          "opus",
		CodecHeader:    CreateHeader(format),
		PacketDuration: int(format.Interval),
	}
	/*
		commandjson, err := json.Marshal(command)
		if err != nil {
			log.Errorf("Problem marshalling command")
			return
		}
		log.Info(string(commandjson))
	*/
	var resp Response
	resp, err = s.SendMessage(command)
	if err != nil {
		log.Errorf("Problem sending message %v", err)
		return
	}
	log.Infof("Response to open channel was %v", resp)
	if resp.Success {
		// Start stream here
		s.Streams[resp.StreamID] = &Stream{
			ID:     resp.StreamID,
			Index:  0,
			Length: 0,
		}
		id = resp.StreamID
		s.Streams[resp.StreamID].Encoder, err = opus.NewEncoder(int(format.Rate), 1, opus.AppVoIP)
		if err != nil {
			log.Errorf("Problem opening opus encoder %v", err)
		}

	} else {
		err = errors.New(resp.Error)
	}
	return
}

func (s *Session) CloseChannel(channel string, id uint32) error {
	command := &StreamCommand{
		Command:    "stop_stream",
		Channel:    channel,
		StreamType: "audio",
		StreamID:   id,
	}

	resp, err := s.SendMessage(command)
	if resp.Success {
		delete(s.Streams, id)
	} else {
		err = errors.New(resp.Error)
	}
	return err
}

func (s *Session) WriteAudio(id uint32, format StreamType, audio *[]int16) error {
	if s.Streams[id] == nil {
		err := errors.New(fmt.Sprintf("Stream %v does not exist!", id))
		return err
	}
	payload := make([]byte, 1000)
	len, err := s.Streams[id].Encoder.Encode(*audio, payload)
	header := make([]byte, 9)
	header[0] = 0x01                            // message type = audio
	binary.BigEndian.PutUint32(header[1:5], id) // Stream ID
	binary.BigEndian.PutUint32(header[5:9], s.Streams[id].Index)
	packet := append(header, payload[0:len]...)
	err = s.WS.WriteMessage(websocket.BinaryMessage, packet)
	return err
}
