package zellochannel

import (
	//	"bytes"
	//	"github.com/damonqin/portaudio"
	"github.com/gorilla/websocket"
	//	"github.com/wernerd/GoRTP/src/net/rtp"

	//"golang.org/x/net/websocket"
	"gopkg.in/hraban/opus.v2"
)

// A session with a Zello server
type Session struct {
	WS                 *websocket.Conn          // The websocket connection handle
	Channels           map[string]ChannelStatus // The most recent status of any subscribe channels
	Sequence           int                      // The current sequence number
	RequestQueue       map[int]chan Response    // Map of channels awaiting responses
	Streams            map[uint32]*Stream       // All ongoing streams
	StatusHandler      func(ChannelStatus)
	StreamStartHandler func(StreamCommand, *Stream)
	StreamStopHandler  func(StreamCommand, *Stream)
	PacketHandler      func(*Stream)
}

// Defines an interface for messages to be sent to the server
type Message interface {
	SetSequence(int)
}

// A logon message - needed to set up communication
type Logon struct {
	Command      string   `json:"command"`
	Seq          int      `json:"seq"`
	AuthToken    string   `json:"auth_token,omitempty"`
	RefreshToken string   `json:"refresh_token,omitempty"`
	Username     string   `json:"username,omitempty"`
	Password     string   `json:"password,omitempty"`
	Channels     []string `json:"channels"`
	ListenOnly   bool     `json:"listen_only"`
}

// Implements the Message interface so sequence can be set
func (logon *Logon) SetSequence(seq int) {
	logon.Seq = seq
}

// A server response to a message
type Response struct {
	Seq          int    `json:"seq"`     // Which message sequence ID does this correspond to
	Success      bool   `json:"success"` // Is this an OK or error?
	RefreshToken string `json:"refresh_token"`
	StreamID     uint32 `json:"stream_id"` // If a stream was opened, the stream ID to use
	Error        string `json:"error"`     // Error message, if Success is false
}

// Command to start or stop a stream.  Can be client to server or server to client
type StreamCommand struct {
	Command        string `json:"command"`
	Seq            int    `json:"seq"`
	Channel        string `json:"channel,omitempty"`
	StreamID       uint32 `json:"stream_id,omitempty"`
	StreamType     string `json:"type,omitempty"`
	Codec          string `json:"codec,omitempty"`
	CodecHeader    string `json:"codec_header,omitempty"`
	PacketDuration int    `json:"packet_duration,omitempty"`
	From           string `json:"from,omitempty"`
	For            string `json:"for,omitempty"`
}

// Set the sequence number of the command
func (command *StreamCommand) SetSequence(seq int) {
	command.Seq = seq
}

// Current status of a channel
type ChannelStatus struct {
	Channel         string `json:"channel"`
	Status          string `json:"status"`
	UsersOnline     int    `json:"users_online"`
	ImageSupport    bool   `json:"images_supported"`
	TextSupport     bool   `json:"texting_supported"`
	LocationSupport bool   `json:"locations_supported"`
}

// Stream header as an object
type StreamType struct {
	Rate     uint16 // Sample rate
	Codec    string // Which codec - always "opus"
	Interval uint8  // The packetization interval in ms
	Frames   uint8  // Number of frames per packet - has to be one for Opus
}

// A decoded stream packet
type Stream struct {
	ID uint32
	//	LastTalk string        // This may be rendundant - streams exist once per TX
	Index   uint32        // The current playout position in frames
	Length  uint32        // The length of the transmission in frames
	Format  StreamType    // Stream format - audio, text, picture, position
	Encoder *opus.Encoder // Handle to the Opus encoder if audio
	Decoder *opus.Decoder // Handle to the Opus decoder if audio
	Audio   []int16       // The uncompressed audio data
	//	PStream    *portaudio.Stream // A handle to a portaudio stream
	//	RTPSession *rtp.Session      // A handle to an RTP session
	//	RTPTime    uint32            //RTP Timestamp
	Buffer []int16 // The last frame of audio that came in
}
