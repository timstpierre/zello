package zellochannel

import (
	b64 "encoding/base64"
	"encoding/binary"
	"encoding/json"
	"time"

	"github.com/gorilla/websocket"
	log "github.com/sirupsen/logrus"
)

func CreateHeader(streamData StreamType) string {
	var header []byte
	header = make([]byte, 4, 4)
	rateBytes := make([]byte, 2, 2)
	binary.LittleEndian.PutUint16(rateBytes, streamData.Rate)
	header[0] = rateBytes[0]
	header[1] = rateBytes[1]
	header[2] = streamData.Frames
	header[3] = streamData.Interval
	return b64.StdEncoding.EncodeToString(header)
}

func DecodeHeader(headerStr string) StreamType {
	var streamData StreamType
	header, err := b64.StdEncoding.DecodeString(headerStr)
	//	log.Infof("Header is %v", header)
	if err != nil {
		log.Errorf("Problem decoding stream header %v", err)
	}
	streamData.Rate = binary.LittleEndian.Uint16(header[:2])
	streamData.Frames = header[2]
	streamData.Interval = header[3]
	return streamData
}

// Send a message to the session
func (s *Session) SendMessage(message Message) (response Response, err error) {
	seq := s.Sequence
	s.Sequence++
	message.SetSequence(seq)
	messagejson, err := json.Marshal(message)
	if err != nil {
		log.Errorf("Problem logging on %v", err)
		return
	}
	log.Debug(string(messagejson))
	s.RequestQueue[seq] = make(chan Response)
	s.WS.WriteMessage(websocket.TextMessage, messagejson)
	for {
		select {
		case response = <-s.RequestQueue[seq]:
			return
			log.Debugf("Got response for req %v", seq)
		case <-time.After(time.Second * 2):
			log.Warnf("Response handler for seq %v timed out", seq)
			return
		}
	}
	log.Infof("message response is %v")
	return
}
