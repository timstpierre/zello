package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	g711 "github.com/pd0mz/go-g711"
	log "github.com/sirupsen/logrus"
	//	"github.com/wernerd/GoRTP/src/net/rtp"
	//	"io/ioutil"
	//	"github.com/dh1tw/gosamplerate"
	chn "bitbucket.org/timstpierre/zello/wschannel"
	"github.com/damonqin/portaudio"
	"math/rand"
	"net"
	"sync"
	"time"
)

var (
	//PacketTicker = time.NewTicker(time.Milliseconds * 20)
	SendBuffer bytes.Buffer
	Speaker    *portaudio.Stream
	IsRX       bool
	TestBuffer = make([]int16, 8000*20/1000)
	TestJB     = make(map[uint16]*[]int16)
	JBLock     sync.Mutex
)

const (
	//	SendAddress     = "224.0.0.3:7000"
	BytesPerSample  = 1
	SamplesPerFrame = 160
)

func SendFrame(session *RTPSession, format chn.StreamType, samples []int16) error {
	var err error
	ratio := format.Rate / 8000
	var payload = make([]int16, len(samples)/int(ratio))
	var sindex int
	for i, v := range samples {
		if i%int(ratio) == 0 {
			payload[sindex] = v
			sindex++
		}
	}

	subframes := int(format.Interval) / 20
	if subframes == 3 {
		chunk := len(payload) / 3
		session.SendPacket(false, false, false, g711.MLawEncode(payload[0:chunk]))
		time.Sleep(10 * time.Millisecond)
		session.SendPacket(false, false, false, g711.MLawEncode(payload[chunk:2*chunk]))
		time.Sleep(10 * time.Millisecond)
		session.SendPacket(false, false, false, g711.MLawEncode(payload[2*chunk:3*chunk]))
	} else if subframes == 2 {
		chunk := len(payload) / 2
		session.SendPacket(false, false, false, g711.MLawEncode(payload[0:chunk]))
		time.Sleep(10 * time.Millisecond)
		session.SendPacket(false, false, false, g711.MLawEncode(payload[chunk:2*chunk]))
	} else {
		session.SendPacket(false, false, false, g711.MLawEncode(payload))
	}

	return err
}

// Get preferred outbound ip of this machine
func GetOutboundIP(destination string) net.UDPAddr {
	conn, err := net.Dial("udp", destination)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return *localAddr
}

// Listen for incoming
func RTPListen(s *chn.Session) {
	a := "224.0.0.4:7000"
	addr, err := net.ResolveUDPAddr("udp", a)
	if err != nil {
		log.Fatal(err)
	}
	l, err := net.ListenMulticastUDP("udp", nil, addr)
	l.SetReadBuffer(1200)
	for {
		b := make([]byte, 1200)

		n, src, err := l.ReadFromUDP(b)
		if err != nil {
			log.Fatal("ReadFromUDP failed:", err)
		}
		log.Debugf("Got UDP packet from %v of length %v contents %v", src, n, b)
		rtp, _ := DecodeRTP(b[:n])
		rtpjson, _ := json.Marshal(rtp)
		log.Debugf("RTP packet is %v", string(rtpjson))
		//		log.Warnf("Payload is %v len %v", rtp.Payload[:160], len(rtp.Payload))

		audio := g711.MLawDecode(rtp.Payload)
		JBLock.Lock()
		TestJB[rtp.Sequence] = &audio
		JBLock.Unlock()

		if !IsRX && rtp.Sequence > 10 {
			IsRX = true
			go StartReceive(rtp.Sequence, s)
		}

	}
}

func StartReceive(s uint16, c *chn.Session) {
	var err error
	var t uint16
	var ps *portaudio.Stream

	t = s
	buffer := make([]int16, 160)
	var streamID uint32
	format := chn.StreamType{
		Rate:     8000,
		Codec:    "opus",
		Interval: 20,
		Frames:   1,
	}
	streamID, err = c.OpenChannel("St. Pierre Port Perry", format)

	time.Sleep(120 * time.Millisecond)
	if *LocalAudio {
		ps, err = portaudio.OpenDefaultStream(0, 1, 8000, 1, &buffer)
		if err != nil {
			log.Errorf("Problem opening speaker %v", err)
		}
		ps.Start()
	}
	log.Infof("Starting playout - buffer is %v packets deep", len(TestJB))

	var emptycount int
	pt := time.NewTicker(time.Millisecond * 20)
	for emptycount < 10 {
		select {

		case <-pt.C:
			if TestJB[t] == nil {
				emptycount++
				TestBuffer = make([]int16, 160)
				log.Warnf("Missing packet at Seq %v", t)
			} else {
				buffer = *TestJB[t]
				log.Infof("Sending Seq %v to speaker", t)
			}

			if *LocalAudio {
				ps.Write()
			}
			c.WriteAudio(streamID, format, &buffer)
			JBLock.Lock()
			delete(TestJB, t)
			JBLock.Unlock()
			t++
		}
	}
	pt.Stop()

	log.Infof("Stopped stream after %v packets", t)
	IsRX = false
}

func DecodeRTP(input []byte) (packet RTPPacket, err error) {
	var byte0 uint8
	byte0 = input[0]
	var byte1 = input[1]
	var CC uint8
	var version uint8

	CC = byte0 &^ 0xfc

	byte0 = byte0 >> 4
	packet.Extension = byte0&1 != 0
	byte0 = byte0 >> 1
	packet.Pad = byte0&1 != 0
	byte0 = byte0 >> 1

	version = byte0
	log.Debugf("RTP packet has version %v", version)

	packet.CC = CC

	packet.PT = byte1 &^ 0x80
	byte1 = byte1 >> 7
	packet.Marker = byte1%1 != 0

	packet.Sequence = binary.BigEndian.Uint16(input[2:4])
	packet.Timestamp = binary.BigEndian.Uint32(input[4:8])
	packet.SSRC = binary.BigEndian.Uint32(input[8:12])

	var csrcIndex uint8

	log.Warnf("packet Contribution Codes size is %v", packet.CC)

	for csrcIndex < CC {
		start := csrcIndex * 4
		end := csrcIndex*4 + 4
		packet.CSRC[csrcIndex] = binary.LittleEndian.Uint32(input[start:end])
		csrcIndex++
	}

	payloadStart := int(CC)*4 + 12
	packet.Payload = input[payloadStart:]

	return
}

func NewRTPSession(dest string, pt uint8, rate uint16) (session *RTPSession, err error) {
	session = &RTPSession{
		SSRC:      rand.Uint32(),
		Timestamp: uint32(rand.Intn(512) * int(rate)),
		Rate:      rate,
		PT:        pt,
	}
	var addr *net.UDPAddr
	addr, err = net.ResolveUDPAddr("udp", dest)
	if err != nil {
		return
	} else {
		session.C, err = net.DialUDP("udp", nil, addr)
	}
	//	session.SendPacket(false, true, false, []byte{})
	return
}

func (session *RTPSession) SendPacket(pad, mark, extension bool, payload []byte) error {
	var err error
	session.C.Write(payload)
	session.Sequence++
	session.Timestamp += uint32(session.Rate)
	packet := RTPPacket{
		Pad:       pad,
		Marker:    mark,
		Extension: extension,
		PT:        session.PT,
		Sequence:  session.Sequence,
		Timestamp: session.Timestamp,
		SSRC:      session.SSRC,
		Payload:   payload,
	}
	if packet.Sequence == 0 {
		packet.Marker = true
	}
	session.C.Write(packet.Encode())
	return err
}

func (session *RTPSession) Close() {
	session.C.Close()
}

type RTPSession struct {
	C         *net.UDPConn
	Sequence  uint16 // Sequence number
	Rate      uint16 // Sample rate - Timestamp will increment by this number every sequence
	Timestamp uint32 // Timestamp
	SSRC      uint32 // Session source identifier
	PT        uint8  // Payload type
}

type RTPPacket struct {
	Pad       bool
	CC        uint8
	Extension bool
	Marker    bool
	PT        uint8
	Sequence  uint16
	Timestamp uint32
	SSRC      uint32
	CSRC      []uint32
	Payload   []byte
}

func (packet RTPPacket) Encode() []byte {
	headersize := 12 + len(packet.CSRC)*4
	//	datasize := headersize + len(packet.Payload)
	header := make([]byte, headersize)
	var byte0 uint8
	var version uint8 = 2
	byte0 = version << 6 // version 2
	if packet.Pad {
		byte0 = byte0 | 0x20
	}
	if packet.Extension {
		byte0 = byte0 | 0x10
	}
	cc := uint8(len(packet.CSRC))
	if cc > 16 {
		cc = 16
	}
	byte0 = byte0 | cc
	header[0] = byte0

	byte1 := packet.PT
	//	byte1 = byte1 << 1
	if packet.Marker {
		byte1 = byte1 | 0x80
	}
	binary.BigEndian.PutUint16(header[2:4], packet.Sequence)
	binary.BigEndian.PutUint32(header[4:8], packet.Timestamp)
	binary.BigEndian.PutUint32(header[8:12], packet.SSRC)

	for k, v := range packet.CSRC {
		start := k*4 + 12
		end := start + 4
		binary.BigEndian.PutUint32(header[start:end], v)
	}
	data := append(header, packet.Payload...)
	/*
		log.Warnf("Encoded RTP packet is %v", data)
		rtp, _ := DecodeRTP(data)
		rtpjson, _ := json.Marshal(rtp)
		log.Warnf("RTP packet is %v", string(rtpjson))
	*/
	return data
}
