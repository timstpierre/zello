package main

import (
	"flag"
	"os"
	"os/signal"

	//	"net/http"
	//	"encoding/json"

	//	"github.com/hraban/opus"
	chn "bitbucket.org/timstpierre/zello/wschannel"
	"github.com/damonqin/portaudio"
	log "github.com/sirupsen/logrus"
	//	"time"
)

var (
	LogLevel   = flag.String("loglevel", "info", "Log Level")
	ServerURL  = flag.String("server", "wss://zello.io/ws", "Zello server URL")
	Username   = flag.String("username", "stpierrebase", "Zello username")
	Password   = flag.String("password", "Il0veptt!", "Zello password")
	Channel    = flag.String("channel", "St. Pierre Port Perry", "Channel to connect to")
	LocalAudio = flag.Bool("localaudio", false, "Enable local audio")
	TestMode   = flag.Bool("testmode", false, "Disable zello connection and test multicast RX")
	AuthToken  = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJXa002ZEdsdGMzUndPakk9LmtSMkE2MldUcDdzKzRIY3JjdEVqeU5Gemt5Z25Qck5jeDNYTk01YUNCd0k9IiwiZXhwIjoxNjQzMjMxMzg2LCJhenAiOiJkZXYifQ==.LRd/8Iew1nK77bz7ncgR9DksDrMu/BWhhwiXq0ymVXSWx4QFXnZT+gv3+cGy3KLUNt16gzWRWWt8MrIG0FLTIMpcEtNkYwwwAhMpPi+M/Goc9kxIllDyRg7aw1S8q9D9KIKQ9LbofgUhbWd2vSK3zqlFt3ckYNGmheA1iT8r3HrUnnKIibjX1gWehAagPGa/yzF6QZZ+Ma5tXe6+3dpjYd11B0nWRumiaVY9RcZXGmQhasli0TfVSjRDiwaoxvjhrGJ1d5q4EDP691F75wq6nUZfrkSvK8CJPEaP2Fzd2l1Ebb9M8dy+1GkUy2d5qxzusQj70uBDLuQSXya97qYV7w=="
)

/*
const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Maximum message size allowed from peer.
	maxMessageSize = 8192

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10
)

// Client is a middleman between the websocket connection and the hub.
type Client struct {
	hub *Hub

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}
*/
func init() {
	flag.Parse()
	lvl, _ := log.ParseLevel(*LogLevel)
	log.SetLevel(lvl)
}

func main() {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	var err error
	var session *chn.Session
	if !*TestMode {
		session, err = chn.Connect(*ServerURL)
		if err != nil {
			log.Fatalf("Problem connecting to server %v", err)
		}
		session.StatusHandler = HandleStatus
		session.StreamStartHandler = HandleStreamStart
		session.StreamStopHandler = HandleStreamStop
		session.PacketHandler = HandlePacket
		err = session.Logon(*Username, *Password, AuthToken, []string{*Channel})
		if err != nil {
			log.Fatalf("Problem with login %v", err)
		}
	}
	if *LocalAudio {
		err := portaudio.Initialize()
		if err != nil {
			log.Fatalf("Problem initializing Portaudio %v", err)
		}
	}

	//time.Sleep(time.Second * 3)
	//	OpenChannel(c)

	go RTPListen(session)
	for {
		select {

		case <-interrupt:
			log.Error("Quitting")
			if !*TestMode {
				session.Disconnect()
			}
			//			if *LocalAudio {
			//				portaudio.Close()
			//			}
			return

		}
	}

}
