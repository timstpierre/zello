package main

import (
	chn "bitbucket.org/timstpierre/zello/wschannel"
	"github.com/damonqin/portaudio"
	log "github.com/sirupsen/logrus"
	//	"github.com/wernerd/GoRTP/src/net/rtp"
)

var (
	PStreams    = make(map[uint32]*portaudio.Stream)
	RTPStreams  = make(map[uint32]*RTPSession)
	SendAddress = "224.0.0.3:7000"
)

func init() {

}

func HandleStreamStart(command chn.StreamCommand, stream *chn.Stream) {
	var err error
	if *LocalAudio {
		PStreams[command.StreamID], err = portaudio.OpenDefaultStream(0, 1, float64(stream.Format.Rate), int(stream.Format.Frames), &stream.Buffer)
		if err != nil {
			log.Errorf("Problem opening Portaudio stream %v", err)
		}

		err = PStreams[command.StreamID].Start()
		if err != nil {
			log.Errorf("Problem Portaudio streamstarting stream %v", err)
		}

		// Preload the buffer while packets come in
		PStreams[stream.ID].Write()
		PStreams[stream.ID].Write()
		PStreams[stream.ID].Write()

	}
	RTPStreams[stream.ID], err = NewRTPSession(SendAddress, 0, 8000) // Start a g711 8KHz stream
	if err != nil {
		log.Errorf("Problem opening RTP stream %v", err)
	}

}

func HandleStreamStop(command chn.StreamCommand, stream *chn.Stream) {
	//	Streams[id].RTPSession.CloseSession()
	log.Infof("Closing stream %v", command.StreamID)
	if *LocalAudio {
		PStreams[command.StreamID].Stop()
		PStreams[command.StreamID].Close()
	}
	RTPStreams[command.StreamID].Close()

}

func HandleStatus(status chn.ChannelStatus) {
	log.Infof("Got channel status from %v using callback", status.Channel)
}

func HandlePacket(stream *chn.Stream) {
	log.Debugf("Got a packet from a stream")
	var err error
	if *LocalAudio {
		err = PStreams[stream.ID].Write()
		if err != nil {
			log.Errorf("Problem writing to Portaudio %v", err)
		}

	}

	err = SendFrame(RTPStreams[stream.ID], stream.Format, stream.Buffer)
	if err != nil {
		log.Errorf("Problem sending with RTP %v", err)
	}

}
