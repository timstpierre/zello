package main

import (
	//	b64 "encoding/base64"
	"encoding/binary"

	"github.com/damonqin/portaudio"
	log "github.com/sirupsen/logrus"
	"gopkg.in/hraban/opus.v2"
)

var (
	Streams = make(map[uint32]*Stream, 16)
)

func StreamStart(command StreamCommand, format StreamType) {
	var err error
	id := command.StreamID

	log.Infof("Creating stream object for ID %v", id)
	Streams[id] = &Stream{
		ID:       command.StreamID,
		LastTalk: command.From,
		Format:   format,
	}

	Streams[id].Decoder, err = opus.NewDecoder(int(format.Rate), 1)
	if err != nil {
		log.Errorf("Problem opening Opus decoder %v", err)
		return
	}
	samples := int(format.Rate) * int(format.Interval) * int(format.Frames) / 1000
	log.Debugf("Recommended buffer size is %v based on format %s", samples, format)
	Streams[id].Buffer = make([]int16, samples)
	if *LocalAudio {
		Streams[id].PStream, err = portaudio.OpenDefaultStream(0, 1, float64(format.Rate), int(format.Frames), &Streams[command.StreamID].Buffer)
		if err != nil {
			log.Errorf("Problem opening Portaudio stream %v", err)
		}
	}
	Streams[id].RTPSession, err = OpenRTP()

	if err != nil {
		log.Errorf("Problem opening RTP stream %v", err)
	}
	if *LocalAudio {
		err = Streams[id].PStream.Start()
		if err != nil {
			log.Errorf("Problem Portaudio streamstarting stream %v", err)
		}

	}
	//	log.Infof("Memory object for stream object %v is %v", id, Streams[id])

}

func StreamStop(id uint32) {
	log.Infof("Stopping stream %v", id)
	log.Infof("Closing stream %v", Streams[id].ID)
	if *LocalAudio {
		Streams[id].PStream.Stop()
		Streams[id].PStream.Close()
	}
	Streams[id].RTPSession.CloseSession()
	delete(Streams, id)
}

func HandleStream(s *Session, messagebyte []byte) {
	//	log.Infof("received stream frame of length %v", len(messagebyte))
	var messagetype uint8
	var stream_id uint32
	var packet_id uint32
	var data []byte
	//	time.Sleep(time.Second / 2)
	messagetype = messagebyte[0]
	if messagetype == 0x01 && len(messagebyte) > 9 {
		stream_id = binary.BigEndian.Uint32(messagebyte[1:5])
		packet_id = binary.BigEndian.Uint32(messagebyte[5:9])
		data = messagebyte[9:]
		log.Debugf("Got stream id %v packet %v of length %v ", stream_id, packet_id, len(data))

		//		s := Streams[stream_id]
		log.Debugf("Decoding to stream handle %v", Streams[stream_id].ID)
		//samples := s.Format.Rate * 1000 * uint16(s.Format.Interval) * uint16(s.Format.Frames) / 1000
		samplesdecoded, err := Streams[stream_id].Decoder.Decode(data, Streams[stream_id].Buffer)
		if err != nil {
			log.Errorf("Problem decoding frame from stream %v %v", stream_id, err)
		} else {
			Streams[stream_id].Audio = append(Streams[stream_id].Audio, Streams[stream_id].Buffer...)
			if *LocalAudio {
				err = Streams[stream_id].PStream.Write()
			}
			if err != nil {
				log.Errorf("Problem writing to Portaudio %v", err)
			}
			err = SendFrame(Streams[stream_id].RTPSession, &Streams[stream_id].RTPTime, Streams[stream_id].Format, Streams[stream_id].Buffer)
			if err != nil {
				log.Errorf("Problem sending with RTP %v", err)
			}
			Streams[stream_id].Index = Streams[stream_id].Index + uint32(samplesdecoded)
			Streams[stream_id].Length += uint32(samplesdecoded)
			log.Debugf("Samples in this packet is %v", samplesdecoded)
		}
	} else {
		log.Errorf("Problem with stream data - type is %v and length is %v", messagetype, len(messagebyte))
	}

}
