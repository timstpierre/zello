package main

import (
	log "github.com/sirupsen/logrus"
	//	b64 "encoding/base64"
	//	"encoding/binary"
	"encoding/json"
)

func HandleMessage(s *Session, messagebyte []byte) {
	var messagemap map[string]interface{}
	err := json.Unmarshal(messagebyte, &messagemap)
	if err != nil {
		log.Errorf("Problem testing message %v", err)
	}
	seq, isreply := messagemap["seq"]
	if isreply {
		seqInt := int(seq.(float64))
		log.Infof("Message is a reply to seq %v", seqInt)
		var response Response
		err = json.Unmarshal(messagebyte, &response)
		if err != nil {
			log.Errorf("Problem unmarshalling reply %v", err)
		}
		s.RequestQueue[seqInt] <- response

	} else {
		switch messagemap["command"] {
		case "on_channel_status":
			log.Info("Channel status message")
			HandleStatus(s, messagebyte)
		case "on_stream_start", "on_stream_stop":
			HandleStreamCommand(s, messagebyte)
		}

	}
}

func HandleStatus(s *Session, message []byte) {
	var status ChannelStatus
	err := json.Unmarshal(message, &status)
	if err != nil {
		log.Errorf("Problem unmarshalling status %v", err)
		return
	}
	log.Warnf("Channel %v is %v with %v users", status.Channel, status.Status, status.UsersOnline)
	s.Channels[status.Channel] = status
}

func HandleStreamCommand(s *Session, message []byte) {
	var command StreamCommand
	err := json.Unmarshal(message, &command)
	if err != nil {
		log.Errorf("Problem unmarshalling stream command %v", err)
		return
	}
	log.Infof("Channel %v command %v from %v ", command.Channel, command.Command, command.From)
	switch command.Command {
	case "on_stream_start":
		//		log.Info("Start playback now")
		StreamFormat := DecodeHeader(command.CodecHeader)
		StreamFormat.Codec = command.Codec
		log.Infof("Header is %v", StreamFormat)
		StreamStart(command, StreamFormat)
	case "on_stream_stop":
		log.Info("Stream stopped")
		StreamStop(command.StreamID)
	}
}

func (s *Session) OpenChannel(channel string) error {
	var err error
	command := StreamCommand{
		Command:        "start_stream",
		Channel:        channel,
		StreamType:     "audio",
		Codec:          "opus",
		CodecHeader:    CreateHeader(StreamType{Rate: 16000, Frames: 1, Interval: 20}),
		PacketDuration: 20,
	}
	/*
		commandjson, err := json.Marshal(command)
		if err != nil {
			log.Errorf("Problem marshalling command")
			return
		}
		log.Info(string(commandjson))
	*/
	var resp Response
	resp, err = s.SendMessage(command)
	if resp.Success {
		// Start stream here
	}
}
